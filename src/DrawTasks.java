import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Label;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Line2D;

import javax.sound.sampled.Line;

public class DrawTasks {
	
	static int yLinex1 = 40;
	static int yLinex2 = 40;
	static int yLineY1 = 40;
	static int yLineY2 = 300;
	
	static int xLineX1 = 40;
	static int xLineX2 = 450;
	static int xLineY1 = 300;
	static int xLineY2 = 300;
	
	static int ySeparationLineDistance = 55;
	static int xSeparationLineDistance = 40;
	
	public void drawTasks(Graphics2D graph2) {
		drawTask1(graph2);
		drawTask2(graph2);
		drawTask3(graph2);
	}
	
	private static void drawTask1(Graphics2D graph2) {	
		int duration = 2;
		int width = xSeparationLineDistance / 5 * duration;
		
		//Task 1 Position 1
		Shape task1p1 = new Rectangle(yLinex1, xLineY2 - 3 * ySeparationLineDistance, width, ySeparationLineDistance);
		graph2.setColor(Color.CYAN);
		graph2.fill(task1p1);
		
		//Task 1 Position 2
		Shape task1p2 = new Rectangle(yLinex1 + 3 * xSeparationLineDistance, xLineY2 - 3 * ySeparationLineDistance, width, ySeparationLineDistance);
		graph2.setColor(Color.CYAN);
		graph2.fill(task1p2);
		
		//Task 1 Position 3
		Shape task1p3 = new Rectangle(yLinex1 + 2 * 3 * xSeparationLineDistance, xLineY2 - 3 * ySeparationLineDistance, width, ySeparationLineDistance);
		graph2.setColor(Color.CYAN);
		graph2.fill(task1p3);
	}
	
	private static void drawTask2(Graphics2D graph2) {	
		int duration = 4;
		int width = xSeparationLineDistance / 5 * duration;
		int task1Width = xSeparationLineDistance / 5 * 2; //because the task1 duration is 2
		
		//Task 2 Position 1
		Shape task2p1 = new Rectangle(yLinex1 + task1Width, xLineY2 - 2 * ySeparationLineDistance, width, ySeparationLineDistance);
		graph2.setColor(Color.yellow);
		graph2.fill(task2p1);

		//Task 2 Position 2
		Shape task2p2 = new Rectangle(yLinex1 + 4 * xSeparationLineDistance, xLineY2 - 2 * ySeparationLineDistance, width, ySeparationLineDistance);
		graph2.setColor(Color.yellow);
		graph2.fill(task2p2);
		
		//Task 2 Position 3
		Shape task2p3 = new Rectangle(yLinex1 + 2 * 4 * xSeparationLineDistance, xLineY2 - 2 * ySeparationLineDistance, width, ySeparationLineDistance);
		graph2.setColor(Color.yellow);
		graph2.fill(task2p3);
	}
	
	private static void drawTask3(Graphics2D graph2) {	
		int duration = 8;
		int width = xSeparationLineDistance / 5 * duration;
		int task1Width = xSeparationLineDistance / 5 * 2; //because the task1 duration is 2
		int task2Width = xSeparationLineDistance / 5 * 4; //because the task1 duration is 4
		
		//Task 2 Position 1
		Shape task2p1 = new Rectangle(yLinex1 + task1Width + task2Width, xLineY2 - ySeparationLineDistance, width, ySeparationLineDistance);
		graph2.setColor(Color.orange);
		graph2.fill(task2p1);

		//Task 2 Position 2
		Shape task2p2 = new Rectangle(yLinex1 + 6 * xSeparationLineDistance + task1Width, xLineY2 - ySeparationLineDistance, width, ySeparationLineDistance);
		graph2.setColor(Color.orange);
		graph2.fill(task2p2);
	}
}
