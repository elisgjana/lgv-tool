import javax.swing.JComponent;
import javax.swing.JFrame;
import java.awt.*;
import java.awt.geom.*;


public class Draw2D extends JFrame{
	
	public static void main(String args[]) {
		new Draw2D();
	}
	
	public Draw2D() {
		this.setSize(500, 500);
		this.setTitle("Drawing Shapes");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.add(new DrawStuff(), BorderLayout.CENTER);
		this.setVisible(true);
	}
	
	private class DrawStuff extends JComponent{
		public void paint(Graphics g) {
			Graphics2D graph2 = (Graphics2D)g;
			graph2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			
			CoordinateSystem cs = new CoordinateSystem();
			cs.draw((Graphics2D)g);

			DrawTasks dt = new DrawTasks();
			dt.drawTasks(graph2);
			
			ActivationArrows aa = new ActivationArrows();
			aa.drawActivationArrows((Graphics2D)g);

		}	
	}	
}

