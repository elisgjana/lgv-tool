import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Line2D;

public class ActivationArrows{
	
	static int yLinex1 = 40;
	static int yLinex2 = 40;
	static int yLineY1 = 40;
	static int yLineY2 = 300;
	
	static int xLineX1 = 40;
	static int xLineX2 = 450;
	static int xLineY1 = 300;
	static int xLineY2 = 300;
	
	static int ySeparationLineDistance = 55;
	static int xSeparationLineDistance = 40;
	
	static int separationUnit = 5;
	
	public static void drawActivationArrows(Graphics2D graph2){
		
		drawInstantTimeArrows(graph2);
		
		drawRestArrows(graph2);
		
	}
	
	private static void drawInstantTimeArrows(Graphics2D graph2) {
		Shape activationArrow1 = new Line2D.Float(yLinex1, xLineY2, yLinex1, xLineY2 - ySeparationLineDistance);

		
		//draw the activationArrow1 down arrow
		Shape aArrDownArrow1Pt1 = new Line2D.Float(yLinex1 + 5, yLineY2 - 10, yLinex2, yLineY2);
		Shape aArrDownArrow1Pt2 = new Line2D.Float(yLinex1 - 5, yLineY2 - 10, yLinex2, yLineY2);
		graph2.setColor(Color.red);
		graph2.draw(activationArrow1);
		graph2.draw(aArrDownArrow1Pt1);
		graph2.draw(aArrDownArrow1Pt2);
		
		
		Shape activationArrow2 = new Line2D.Float(yLinex1, xLineY2 - ySeparationLineDistance, yLinex1, xLineY2 - 2 * ySeparationLineDistance);
		
		//draw the activationArrow2 down arrow
		Shape aArrDownArrow2Pt1 = new Line2D.Float(yLinex1 + 5, yLineY2 - 10 - ySeparationLineDistance, yLinex2, yLineY2 - ySeparationLineDistance);
		Shape aArrDownArrow2Pt2 = new Line2D.Float(yLinex1 - 5, yLineY2 - 10 - ySeparationLineDistance, yLinex2, yLineY2 - ySeparationLineDistance);
		graph2.setColor(Color.orange);
		graph2.draw(activationArrow2);
		graph2.draw(aArrDownArrow2Pt1);
		graph2.draw(aArrDownArrow2Pt2);
		
		
		Shape activationArrow3 = new Line2D.Float(yLinex1, xLineY2 - 2 * ySeparationLineDistance, yLinex1, xLineY2 - 3 * ySeparationLineDistance);
		
		//draw the activationArrow2 down arrow
		Shape aArrDownArrow3Pt1 = new Line2D.Float(yLinex1 + 5, yLineY2 - 10 - 2 * ySeparationLineDistance, yLinex2, yLineY2 - 2 * ySeparationLineDistance);
		Shape aArrDownArrow3Pt2 = new Line2D.Float(yLinex1 - 5, yLineY2 - 10 - 2 * ySeparationLineDistance, yLinex2, yLineY2 - 2 * ySeparationLineDistance);
		graph2.setColor(Color.blue);
		graph2.draw(activationArrow3);
		graph2.draw(aArrDownArrow3Pt1);
		graph2.draw(aArrDownArrow3Pt2);	
	}
	
	private static void drawRestArrows(Graphics2D graph2) {

		/*
		 * TASK 1 ARROWS
		 */
		
		//TASK 1 ARROW 1
		Shape tast1Arrow1 = new Line2D.Float(yLinex1 + 3 * xSeparationLineDistance, xLineY2 - 2 * ySeparationLineDistance, yLinex1 + 3 * xSeparationLineDistance, xLineY2 - 3 * ySeparationLineDistance);
		
		//draw the activationArrow2 down arrow
		Shape task1DownArr1Pt1 = new Line2D.Float(yLinex1 + 5 + 3 * xSeparationLineDistance, yLineY2 - 10 - 2 * ySeparationLineDistance, yLinex2 + 3 * xSeparationLineDistance, yLineY2 - 2 * ySeparationLineDistance);
		Shape task1DownArr1Pt2 = new Line2D.Float(yLinex1 - 5 + 3 * xSeparationLineDistance, yLineY2 - 10 - 2 * ySeparationLineDistance, yLinex2 + 3 * xSeparationLineDistance, yLineY2 - 2 * ySeparationLineDistance);
		graph2.setColor(Color.blue);
		graph2.draw(tast1Arrow1);
		graph2.draw(task1DownArr1Pt1);
		graph2.draw(task1DownArr1Pt2);	
		
		//TASK 1 ARROW 2
		Shape tast1Arrow2 = new Line2D.Float(yLinex1 + 2 * 3 * xSeparationLineDistance, xLineY2 - 2 * ySeparationLineDistance, yLinex1 + 2 * 3 * xSeparationLineDistance, xLineY2 - 3 * ySeparationLineDistance);
		
		//draw the activationArrow2 down arrow
		Shape task1DownArr2Pt1 = new Line2D.Float(yLinex1 + 5 + 2 * 3 * xSeparationLineDistance, yLineY2 - 10 - 2 * ySeparationLineDistance, yLinex2 + 2 * 3 * xSeparationLineDistance, yLineY2 - 2 * ySeparationLineDistance);
		Shape task1DownArr2Pt2 = new Line2D.Float(yLinex1 - 5 + 2 * 3 * xSeparationLineDistance, yLineY2 - 10 - 2 * ySeparationLineDistance, yLinex2 + 2 * 3 * xSeparationLineDistance, yLineY2 - 2 * ySeparationLineDistance);
		graph2.setColor(Color.blue);
		graph2.draw(tast1Arrow2);
		graph2.draw(task1DownArr2Pt1);
		graph2.draw(task1DownArr2Pt2);
		
		/*
		 * TASK 2 ARROWS
		 */
		
		//TASK 2 ARROW 1
		Shape tast2Arrow1 = new Line2D.Float(yLinex1 + 4 * xSeparationLineDistance, xLineY2 - ySeparationLineDistance, yLinex1 + 4 * xSeparationLineDistance, xLineY2 - 2 * ySeparationLineDistance);
		
		//draw the activationArrow2 down arrow
		Shape task2DownArr1Pt1 = new Line2D.Float(yLinex1 + 5 + 4 * xSeparationLineDistance, yLineY2 - 10 - ySeparationLineDistance, yLinex2 + 4 * xSeparationLineDistance, yLineY2 - ySeparationLineDistance);
		Shape task2DownArr1Pt2 = new Line2D.Float(yLinex1 - 5 + 4 * xSeparationLineDistance, yLineY2 - 10 - ySeparationLineDistance, yLinex2 + 4 * xSeparationLineDistance, yLineY2 - ySeparationLineDistance);
		graph2.setColor(Color.orange);
		graph2.draw(tast2Arrow1);
		graph2.draw(task2DownArr1Pt1);
		graph2.draw(task2DownArr1Pt2);
		
		//TASK 2 ARROW 2
		Shape tast2Arrow2 = new Line2D.Float(yLinex1 + 2 * 4 * xSeparationLineDistance, xLineY2 - ySeparationLineDistance, yLinex1 + 2 * 4 * xSeparationLineDistance, xLineY2 - 2 * ySeparationLineDistance);
		
		//draw the activationArrow2 down arrow
		Shape task2DownArr2Pt1 = new Line2D.Float(yLinex1 + 5 + 2 * 4 * xSeparationLineDistance, yLineY2 - 10 - ySeparationLineDistance, yLinex2 + 2 * 4 * xSeparationLineDistance, yLineY2 - ySeparationLineDistance);
		Shape task2DownArr2Pt2 = new Line2D.Float(yLinex1 - 5 + 2 * 4 * xSeparationLineDistance, yLineY2 - 10 - ySeparationLineDistance, yLinex2 + 2 * 4 * xSeparationLineDistance, yLineY2 - ySeparationLineDistance);
		graph2.setColor(Color.orange);
		graph2.draw(tast2Arrow2);
		graph2.draw(task2DownArr2Pt1);
		graph2.draw(task2DownArr2Pt2);
		
		/*
		 * TASK 3 ARROWS
		 */	
		
		//TASK 3 ARROW 1
		Shape tast3Arrow1 = new Line2D.Float(yLinex1 + 6 * xSeparationLineDistance, xLineY2 , yLinex1 + 6 * xSeparationLineDistance, xLineY2 -  ySeparationLineDistance);
		
		//draw the activationArrow2 down arrow
		Shape task3DownArr1Pt1 = new Line2D.Float(yLinex1 + 5 + 6 * xSeparationLineDistance, yLineY2 - 10 , yLinex2 + 6 * xSeparationLineDistance, yLineY2 );
		Shape task3DownArr1Pt2 = new Line2D.Float(yLinex1 - 5 + 6 * xSeparationLineDistance, yLineY2 - 10 , yLinex2 + 6 * xSeparationLineDistance, yLineY2 );
		graph2.setColor(Color.red);
		graph2.draw(tast3Arrow1);
		graph2.draw(task3DownArr1Pt1);
		graph2.draw(task3DownArr1Pt2);
	}

}
