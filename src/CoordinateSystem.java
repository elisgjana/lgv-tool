import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Line2D;

public class CoordinateSystem {
	
	static int yLinex1 = 40;
	static int yLinex2 = 40;
	static int yLineY1 = 40;
	static int yLineY2 = 300;
	
	static int xLineX1 = 40;
	static int xLineX2 = 450;
	static int xLineY1 = 300;
	static int xLineY2 = 300;
	
	static int ySeparationLineDistance = 55;
	static int xSeparationLineDistance = 40;
	
	public void draw(Graphics2D graph2) {		
		Shape yAxisLine = new Line2D.Float(yLinex1, yLineY1, yLinex2, yLineY2);
		Shape xAxisLine = new Line2D.Float(xLineX1, xLineY1, xLineX2, xLineY2);
		graph2.draw(yAxisLine);
		graph2.draw(xAxisLine);
		
		//here we draw the arrows of the coordinative system
		
		//right arrow
		Shape rArrowPt1 = new Line2D.Float(xLineX2 - 10, xLineY1 - 5, xLineX2, xLineY1);
		Shape rArrowPt2 = new Line2D.Float(xLineX2 - 10, xLineY1 + 5, xLineX2, xLineY1);
		graph2.draw(rArrowPt1);
		graph2.draw(rArrowPt2);
		
		//upper arrow
		Shape uArrowPt1 = new Line2D.Float(yLinex1 - 5, yLineY1 + 10, yLinex2, yLineY1);
		Shape uArrowPt2 = new Line2D.Float(yLinex1 + 5, yLineY1 + 10, yLinex2, yLineY1);
		graph2.draw(uArrowPt1);
		graph2.draw(uArrowPt2);
		
		
		//YAxis separation segments
		Shape ySep1 = new Line2D.Float(yLinex1 - 4, xLineY2 - ySeparationLineDistance, yLinex1 + 4, xLineY2 - ySeparationLineDistance);
		Shape ySep2 = new Line2D.Float(yLinex1 - 4, xLineY2 - 2 * ySeparationLineDistance, yLinex1 + 4, xLineY2 - 2 * ySeparationLineDistance);
		Shape ySep3 = new Line2D.Float(yLinex1 - 4, xLineY2 - 3 * ySeparationLineDistance, yLinex1 + 4, xLineY2 - 3 * ySeparationLineDistance);
		graph2.draw(ySep1);
		graph2.draw(ySep2);
		graph2.draw(ySep3);
		
		//YAxis separation segments
		Shape xSep1 = new Line2D.Float(xLineX1 + xSeparationLineDistance, xLineY1 - 5, xLineX1 + xSeparationLineDistance, xLineY1 + 5);
		Shape xSep2 = new Line2D.Float(xLineX1 + 2 * xSeparationLineDistance, xLineY1 - 5, xLineX1 + 2 * xSeparationLineDistance, xLineY1 + 5);
		Shape xSep3 = new Line2D.Float(xLineX1 + 3 * xSeparationLineDistance, xLineY1 - 5, xLineX1 + 3 * xSeparationLineDistance, xLineY1 + 5);
		Shape xSep4 = new Line2D.Float(xLineX1 + 4 * xSeparationLineDistance, xLineY1 - 5, xLineX1 + 4 * xSeparationLineDistance, xLineY1 + 5);
		Shape xSep5 = new Line2D.Float(xLineX1 + 5 * xSeparationLineDistance, xLineY1 - 5, xLineX1 + 5 * xSeparationLineDistance, xLineY1 + 5);
		Shape xSep6 = new Line2D.Float(xLineX1 + 6 * xSeparationLineDistance, xLineY1 - 5, xLineX1 + 6 * xSeparationLineDistance, xLineY1 + 5);
		Shape xSep7 = new Line2D.Float(xLineX1 + 7 * xSeparationLineDistance, xLineY1 - 5, xLineX1 + 7 * xSeparationLineDistance, xLineY1 + 5);
		Shape xSep8 = new Line2D.Float(xLineX1 + 8 * xSeparationLineDistance, xLineY1 - 5, xLineX1 + 8 * xSeparationLineDistance, xLineY1 + 5);
		graph2.draw(xSep1);
		graph2.draw(xSep2);
		graph2.draw(xSep3);
		graph2.draw(xSep4);
		graph2.draw(xSep5);
		graph2.draw(xSep6);
		graph2.draw(xSep7);
		graph2.draw(xSep8);
	}
}
